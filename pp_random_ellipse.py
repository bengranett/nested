import numpy
import sys
import pp
import time

def get_random_ellipse(means,covmat,width):

    # get eigen vectors and values
    w, v = numpy.linalg.eig(covmat)

    mod = 2
    while mod > 1:
        # generate points inside hypercube
        ran = numpy.random.uniform(-w*width,w*width)
        # only keep if they fall inside ellipse
        mod = sum((ran/(w*width))**2)

    # rotate points
    ran = numpy.dot(v,ran)

    # recentre
    ran = ran + means
    return ran

def like_func(x, y):
    mu = 1.5
    mu2 = 2.5
    sig = 1.0
    lnlike = -0.5*numpy.sum( ((y - mu)/sig)**2 ) - 0.5*numpy.log(2*numpy.pi) - numpy.sum(numpy.log(sig))
    lnlike2 = -0.5*numpy.sum( ((x - mu2)/sig)**2 ) - 0.5*numpy.log(2*numpy.pi) - numpy.sum(numpy.log(sig))
    return -lnlike + lnlike2

def sample_ellipse(Lmin, means, covmat, width):
    like = 0
    while like > Lmin:
        trial = get_random_ellipse(means,covmat,width)
        x, y = trial
        like = like_func(x, y)
    return [trial, like]

################################################################
#**************************************************************#
################################################################
# Initiate parallel processes
# tuple of all parallel python servers to connect with
ppservers = ()
if len(sys.argv) > 1:
    ncpus = int(sys.argv[1])
    # Creates jobserver with ncpus workers                                     
    job_server = pp.Server(ncpus, ppservers=ppservers)
else:
    # Creates jobserver with automatically detected number of workers          
    job_server = pp.Server(ppservers=ppservers)

# print the number of cpus found
print "Starting pp with", job_server.get_ncpus(), "workers"
################################################################
#**************************************************************#
################################################################

# some initial stuff to start with
means = [2.5, 1.4]
covmat = [ 8.0, 1.5 ], [1.5,   1.0]
covmat = numpy.asarray(covmat)

# size of ellipse (1 sigma)
width  = numpy.sqrt(2.30)

# number of points to calculate in parallel
nmod = 12

# inputs for job
#inputs = [(means, covmat, width) for i in range(nmod)]
#print inputs

start_time = time.time()

Lmin = -20.0

# loop over jobs
for i in range(nmod):
    # same command sent to each core 
    job = job_server.submit(sample_ellipse, (Lmin, means, covmat, width,),(get_random_ellipse, like_func),("numpy",))
    res = job()
    print res


print "Time elapsed: ", time.time() - start_time, "s"
job_server.print_stats()
