import numpy as N

import matplotlib
matplotlib.rc_context(rc={'backend': 'GTKAgg'})
import pylab


def ellipse_picker(center,covmat, relax=1,s=1,point=None,n=1):
    """Generate uniform random samples from within the volume of a hyper ellipse

    Inputs:
    center  - vector of length dim
    covmat  - (dim,dim) shape array giving covariance matrix defining the ellipse
    s       - optional scale radius
    relax   - factor to scale the radius
    point   - optionally set the scale by specifying a point on the surface of the hyperellipse
    n       - number of samples to draw

    Outputs:
    (n,dim) shape array containing the sampled points
    """
    import numpy as N
    dim = len(center)

    # run special case for 1 dimension case
    if dim==1:
        ran = N.random.uniform(-1,1,n)
        s *= covmat
        if not point==None:
            s = point-center
        ran *= s*relax
        ran += center
        return N.array([ran])

    # For higher dimensions get eigen vectors and values
    w, v = N.linalg.eig(covmat)

    # uniform sample from a unit hypersphere
    ran = ballpicker(dim=dim, n=n).T

    # compute scale radius such that ellipse crosses the point
    if not point==None:
        point = N.array(point)
        c = N.dot(v.T, point-center)/w
        s = N.sqrt(N.sum(c*c))

    # rescale by eigen values
    ran *= w*relax*s

    # rotate points
    ran = N.dot(v,ran.T)

    # recentre
    return ran.T + center



def testellipse(point=(0,10)):
    """ """
    pylab.subplot(111,aspect='equal')
    means = [0.5,0]
    covmat = [1,0.5],[0.5,2.]

    x,y = ellipse_picker(means, covmat, point=point, n=100000).T

    #pylab.plot(x,y,",")

    pylab.hexbin(x,y)
    pylab.axvline(point[0],c='y')
    pylab.axhline(point[1],c='y')

    x,y = N.random.multivariate_normal(means,covmat,10000).T
    pylab.plot(x,y,".",alpha=0.1)


    pylab.show()


def ballpicker(radius=1, dim=2, n=100):
    """ Uniform sample from the volume enclosed by a hypersphere"""
    import numpy as N
    assert(dim>0)

    # sample radius with p(r)~r^d
    # this can be constructed from a uniform distribution
    u = N.random.uniform(0,1,n)**(1./dim)

    # sample from the surface of a hypersphere
    # see http://mathworld.wolfram.com/HyperspherePointPicking.html
    w = N.random.normal(0,1,(dim,n))
    norm = N.sqrt(N.sum(w*w, axis=0))

    xyz = w/norm*u*radius

    return xyz


def ballpicker_simple(radius=1, dim=2, n=100, batchfrac=0.01):
    """ Uniform sample from the volume enclosed by a hypersphere 
    by drawing from a hypercube and rejecting points with r>radius
    """
    assert(dim>0)

    xyz = []

    batchsize = int(n*batchfrac)
    count = 0
    tot = 0
    while count<n:
        c = N.random.uniform(-1,1,(dim,batchsize))
        r2 = N.sum(c*c, axis=0)
        ii = r2<1
        xyz.append(c[:,ii])
        count += N.sum(ii)
        tot += batchsize

    xyz = N.hstack(xyz)
    xyz *= radius

    print "dim=%i discard rate"%dim,1-count*1./tot


    return xyz

    
def test(dim=2, n=1000000):
    """Compare the distribution of x samples from the two methods."""

    for dim in [1,2,3,4,5]:
        x=ballpicker_simple(radius=2,dim=dim,n=n)[0]
        h,e=N.histogram(x,bins=100,density=True)
        pylab.plot(e[:-1],h,c='k')

        x=ballpicker(radius=2,dim=dim,n=n)[0]
        h,e=N.histogram(x,bins=100,density=True)
        pylab.plot(e[:-1],h,c='r')

    pylab.show()

if __name__=="__main__":
    #test(7)
    testellipse()
