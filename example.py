import matplotlib
matplotlib.rc_context(rc={'backend': 'GTKAgg'})
import pylab

import numpy as N
import nestednd


class myWalker (nestednd.Walker):
    """ this will describe my problem """
    
    def lnlike(self, x):
        """Compute the log likelihood at point x

        the parameters are 2 means and 2 sigmas of the gaussians

        """

        mux,muy,sigx,sigy = x
        mu = mux,muy
        sig = sigx,sigy

        d = N.transpose(self.data)  # x,y

        k = len(d[0])

        l = 0.0
        for i in range(len(mu)):
            l += -0.5*N.sum( ((mu[i] - d[i])/sig[i])**2 )  - k*N.sum(N.log(sig[i]))


        return l


# 2 dimensional problem


# generate data points by drawing from a gaussian
mean = [1,5.]
sig = [1.5,1.]

truth = N.concatenate([mean,sig])

npoints = 100
x = N.random.normal(mean[0],sig[0],npoints)
y = N.random.normal(mean[1],sig[1],npoints)
data = N.transpose([x,y])


param_limits = [[0,2],  # mean 1
                [4,6],  # mean 2
                [0,2],  # sigma 1
                [0,2]]  # sigma 2

W = myWalker(data, param_limits)


Nest = nestednd.NestedSampler(nlive=500,walker = W,precision=.3e-2,relax=1.1,picker='metropolis')

Nest.go()
mean,sig = Nest.best()
chain = Nest.all_samples.T
weights = Nest.weights



# plot the results

bands = []
for i in range(4):
    bands.append(N.linspace(mean[i]-5*sig[i], mean[i]+5*sig[i], 30))


for i in range(4):
    for j in range(4):

        if i>j: continue

        pylab.subplot(4,4,i+4*j+1)


        if i==j:
            h,e = N.histogram(chain[i],weights=weights,bins=bands[i],density=True)
            huw,e = N.histogram(chain[i],bins=bands[i],density=True)
            pylab.plot((e[1:]+e[:-1])/2., h, c='k')
            pylab.plot((e[1:]+e[:-1])/2., huw, c='g')

            pylab.axvline(truth[i],c='b')
            pylab.xlim(bands[i][0],bands[i][-1])
        else:
            h,ex,ey=N.histogram2d(chain[j],chain[i],weights=weights,bins=(bands[j],bands[i]))
            pylab.scatter(chain[i],chain[j],edgecolors='none',facecolor='dodgerblue',alpha=0.5)
            pylab.contour(h,extent=(ey[0],ey[-1],ex[0],ex[-1]),colors='k')
        
            pylab.scatter(truth[i],truth[j],marker='*',s=100,facecolor='orange',zorder=100)

            pylab.xlim(bands[i][0],bands[i][-1])
            pylab.ylim(bands[j][0],bands[j][-1])

pylab.savefig("mcmc.png")

pylab.show()
