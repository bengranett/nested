import sys
import time
import numpy as N


class Walker:
    def __init__(self, data=None, param_limits=None):
        """ Initialize """
        self.data = data                      # this is the data vector
        self.param_limits = param_limits      # hard parameter limits
        self.dim = len(data)                  # this is the number of data points
        self.nparam = len(param_limits)       # number of parameters

    def lnlike(self, x):
        """ n-dimensional gaussian likelihood"""
        sig = 1
        d = N.transpose(self.data)
    
        k = len(d[0])

        l = 0.0
        for i in range(self.dim):
            l += -0.5*numpy.sum( ((x[i] - d[i])/sig)**2 ) - 0.5*k*numpy.log(2*numpy.pi) - numpy.sum(numpy.log(sig))

        return l

    def draw_sample(self, Lmin, point, center, covmat, relax, maxloops=10000, npoints=1):
        """ find a point with higher likelihood by uniform sampling from a hyperellipse """
        trial_points = []
        trial_like = []
        counter = 0
        while len(trial_points) < npoints:
            counter += 1
            if counter > maxloops: break

            # generate a sample that meets the hard prior limits
            loop = 0
            while True:            
                if loop > maxloops:
                    print >>sys.stderr, "**** error: can't find a point inside the prior",loop
                    break
                loop += 1
                trial = ellipse_picker(center, covmat, point=point, relax=relax, n=1)[0]
                point_outside = False
                for i in range(len(trial)):
                    if trial[i]<self.param_limits[i][0] or  trial[i]>self.param_limits[i][1]:
                        point_outside=True
                if not point_outside: break


            like = self.lnlike(trial)
            if like > Lmin:
                trial_points.append(trial)
                trial_like.append(like)


        accept_rate = len(trial_points)*1./counter

        return trial_points, trial_like, accept_rate

    def draw_sample_metropolis(self, Lmin, point, lpoint, covmat, 
                               stepsize=1):
        """ Find a point with higher likelihood by using the metropolis hastings algorithm
        Lmin -- minimum value of lnlike that we must be above
        point -- starting point
        lpoint -- lnlike at the starting point
        covmat -- livepoints covmat used to make a smart step
        """


        accept_rate = 0
        loop = 0
        count = 0
        restart = 0
        noutside = 0
        while True:

            if noutside > 100:
                print "Fatal error!"
                print "cannot find a new point that is inside the prior!"
                print "stepsize:",stepsize
                print "cmat:",covmat
                print "point:",p_next
                exit(-1)

            # make a step according to the covariance
            p_next = N.random.multivariate_normal(point, covmat*stepsize**2)

            # determine if the point is inside the prior
            outside = False
            for i in range(self.nparam):
                a,b = self.param_limits[i]
                if p_next[i]<a or p_next[i]>b:
                    outside = True
                    break
            if outside:
                noutside += 1
                continue

            loop += 1
            count += 1
            
            # compute likelihood at this point

            l_next = self.lnlike(p_next)

            if l_next > Lmin:
                accept_rate += 1
                break


        accept_rate = accept_rate*1./count


        try:
            assert(l_next>Lmin)
        except:
            print "failed",Lmin,l
            raise

        return [p_next],[l_next],accept_rate
            
            


    def draw_sample_from_prior(self, npoints):
        """ Draw random points from the hard prior given by the parameter limits. """
        x = []
        for i in range(self.nparam):
            a,b = self.param_limits[i]
            x.append(  N.random.uniform(a,b,(npoints)) )
        return N.transpose(x)




class NestedSampler:
    
    def __init__(self,  walker=None, 
                 nlive=1000, nreject=1, precision=1e-2, relax=1.1, picker='metropolis'):
        """ 
        like -- function to evaluate likelihood
        like_args -- arguments required by the function

        nlive -- number of active points
        nreject -- number of points to randomize on each iteration
        precision -- sets stopping point
        relax -- scale radius for hyperellipse point picker
       
        """
        self.picker = picker
        self.walker = walker
        self.precision = precision
        self.relax = relax
        self.nlive = nlive
        self.nmod = nreject
        self.all_samples = []
        self.sample_lnlike = []
        self.weights = []

        self.nparam = walker.nparam




    def go(self):
        """ do it """

        print "~~~~ starting nested sampler ~~~~~~`"
        print "     nlive = %i"%self.nlive
        print "  n reject = %i"%self.nmod

        # generate initial sampling over a uniform prior
        livepoints = self.walker.draw_sample_from_prior(self.nlive)

        # compute the likelihood at these points
        print ">> computing initial likelihood values"
        lnlike_vals = N.zeros(self.nlive)
        for i in range(self.nlive):
            lnlike_vals[i] = self.walker.lnlike(livepoints[i])

        print ">> now running in loops"
        count = 0
        covlist = []
        like_list = []
        evidence = 10000
        evidence_next = 0
        remaining_evidence = 10000

        sig_to_noise = []

        # this is the step accounting for nreject points
        # (see Henderson & Goggans 2014)
        logstep = N.sum(1./(self.nlive - N.arange(self.nmod)))

        stop = False
        stop_counter = 0

        while not stop:
            count += 1

            # print a helpful message
            if not count%100: 
                print "~~~~~~~~~ iteration",count
                #print "evidence:",evidence*1./tot_evidence
                #print "remaining",remaining_evidence
                print "last sample",livepoints[ind_lowest], lnlike_vals[ind_lowest], lnlike_vals.max()
                print "sigma",N.std(livepoints,axis=0)

        
            # identify the n lowest likelihood points
            ind_low = N.argpartition(lnlike_vals, self.nmod)[:self.nmod]
            ind_lowest = ind_low[-1]

            # compute p step
            step = N.exp(-(count-1)*logstep) - N.exp(-count*logstep)

            # compute statistics of the live points
            mean = N.mean(livepoints, axis=0)
            cov = N.cov(livepoints, rowvar=0)

            # draw sample with higher likelihood
            trial_points = []
            trial_like = []
            accept_rate = []

            for i in range(self.nmod):
                if self.picker=='ellipse':
                    # ellipse picker
                    p,l,r = self.walker.draw_sample(lnlike_vals[ind_lowest], livepoints[ind_lowest], mean, cov, self.relax)
                elif self.picker=='metropolis':
                    # metropolis picker
                    pick = N.random.randint(0,self.nlive) # select a random point from the live points
                    p,l,r = self.walker.draw_sample_metropolis(lnlike_vals[ind_lowest], livepoints[pick], lnlike_vals[pick], cov)
                else:
                    print "unknown picker!",self.picker
                    exit(-1)

                if len(p)==0:
                    stop = True
                    print "********* warning! unable to draw next point", cov,"***********"
                    break
                
                trial_points.append(p[0])
                trial_like.append(l[0])
                accept_rate.append(r)

            a = N.mean(accept_rate)
            if a < 0.1:
                print ">  warning: mean accept rate is low (%g)"%a
                print accept_rate

            if stop:
                break

            w = step*N.exp(lnlike_vals[ind_low])
            if count==1:
                self.all_samples = livepoints[ind_low]
            else:
                self.all_samples = N.vstack([self.all_samples, livepoints[ind_low]])

            self.weights = N.concatenate([self.weights,w])
            self.sample_lnlike = N.concatenate([self.sample_lnlike, lnlike_vals[ind_low]])


            # update the live points with new draw
            for i in range(self.nmod):
                livepoints[ind_low[i]] = trial_points[i]
                lnlike_vals[ind_low[i]] = trial_like[i]


            # keep count of the evidence
            evidence += N.sum(N.exp(self.sample_lnlike)*self.weights)
            remaining_evidence = N.exp(N.max(lnlike_vals))*N.exp(-count*1./self.nlive)
            tot_evidence = evidence + remaining_evidence
            
            if tot_evidence>0:
                remaining_evidence /= tot_evidence


            # compute mean and std deviation to determine stopping point
            if N.sum(self.weights)<1e-10:
                mu = N.average(self.all_samples, axis=0)
                var =  N.average((self.all_samples - mu)**2, axis=0)
            else:
                mu = N.average(self.all_samples, weights=self.weights, axis=0)
                var =  N.average((self.all_samples - mu)**2, weights=self.weights, axis=0)


            sig = N.std(livepoints,axis=0)
            r = N.mean(sig/N.sqrt(var))

            sig_to_noise.append(mu/var**.5)
            if count>1:
                dvar = self.nparam*N.mean(N.abs(sig_to_noise[-1]-sig_to_noise[-2]))
                if not count%100: 
                    print "dvar",dvar,r
                    print sig
                    print var**.5
                #if dvar<self.precision:
                if r<self.precision:
                    stop_counter += 1
                else:
                    stop_counter = 0
                
                if stop_counter>10:
                    print ">> reached precision limit 10 loops in a row"
                    stop = True


        self.sig_to_noise = sig_to_noise

        # weight remaining points
        # sort by increasing likelihood
        order = N.argsort(lnlike_vals)
        lnlike_vals = lnlike_vals[order]
        livepoints = livepoints[order]

        # compute ranks and weight
        rank = N.arange(len(lnlike_vals))
        x = N.exp(-rank*1./self.nlive)
        step = x[:-1]-x[1:]
        step = N.concatenate([step,[step[-1]]])

        print "sum of weights",N.sum(self.weights)

        w = step*N.exp(lnlike_vals)
        if w[0]>0:
            w *= self.weights[-1]/w[0]   # not sure about normalization, but match it to previously computed weights
        else:
            w *= 0.

        self.all_samples = N.vstack([self.all_samples, livepoints])
        self.weights = N.concatenate([self.weights,w])
        self.sample_lnlike = N.concatenate([self.sample_lnlike, lnlike_vals])


        # normalize weights by total evidence
        tot = N.sum(self.weights*N.exp(self.sample_lnlike))
        if tot>0:
            self.weights /= tot
            self.evidence = tot

            print "*** total evidence", self.evidence

        print " >> picker", self.picker
        print " >> finished %i loops"%count
        print " >> got this many samples:",len(self.all_samples)



    def best(self):
        """ """
        mu = N.average(self.all_samples, weights=self.weights, axis=0)
        var =  N.average((self.all_samples - mu)**2, weights=self.weights, axis=0)

        print "---- best ----"
        for i in range(len(mu)):
            print i+1, mu[i],var[i]**.5

        return mu,var**.5

def test(dim=2):
    """ """

    import matplotlib
    matplotlib.rc_context(rc={'backend': 'GTKAgg'})
    import pylab



    npoints = 1
    sigma = N.ones((dim,npoints))
    mu = N.ones((dim,npoints))


    walker = Walker(mu)


    param_limits = []
    for i in range(dim):
        param_limits.append((-5,6))

    Nest = NestedSampler(dim, param_limits, walker=walker)
    

    Nest.go()
    mu,sig = Nest.best()
    
    print "should be",sigma[0]/N.sqrt(npoints)


    chain = Nest.all_samples
    weights = Nest.weights


    ext = []
    for i in range(len(mu)):
        ext.append((mu[i]-3*sig[i], mu[i]+3*sig[i]))

    hw,e=N.histogram(chain.T[0],weights=weights,bins=N.linspace(ext[0][0],ext[0][1],50),density=True)
    pylab.plot(e[:-1],hw)


    h,e2=N.histogram(chain.T[0],bins=N.linspace(ext[0][0],ext[0][1],50),density=True)
    pylab.plot(e2[:-1],h)

    x = N.linspace(-2,3,100)

    if dim==1:
        lny = []
        for i in range(len(x)):
            lny.append(walker.lnlike( N.array([x[i]]) ) )
        
        pylab.semilogy(x,N.exp(lny),dashes=[4,3],c='k')
    

    pylab.show()




def ellipse_picker(center,covmat, relax=1,s=1,point=None,n=1):
    """Generate uniform random samples from within the volume of a hyper ellipse

    Inputs:
    center  - vector of length dim
    covmat  - (dim,dim) shape array giving covariance matrix defining the ellipse
    s       - optional scale radius
    relax   - factor to scale the radius
    point   - optionally set the scale by specifying a point on the surface of the hyperellipse
    n       - number of samples to draw

    Outputs:
    (n,dim) shape array containing the sampled points
    """
    import numpy as N
    dim = len(center)

    # run special case for 1 dimension case
    if dim==1:
        ran = N.random.uniform(-1,1,n)
        s *= covmat
        if not point==None:
            s = point-center
        ran *= s*relax
        ran += center
        return N.array([ran])

    # For higher dimensions get eigen vectors and values
    w, v = N.linalg.eig(covmat)

    # uniform sample from a unit hypersphere
    ran = ballpicker(dim=dim, n=n).T

    # compute scale radius such that ellipse crosses the point
    if not point==None:
        point = N.array(point)
        c = N.dot(v.T, point-center)/w
        s = N.sqrt(N.sum(c*c))
        if s > 10:
            s = 10

    # rescale by eigen values
    ran *= w*relax*s

    # rotate points
    ran = N.dot(v,ran.T)

    # recentre
    return ran.T + center

def ballpicker(radius=1, dim=2, n=100):
    """ Uniform sample from the volume enclosed by a hypersphere"""
    import numpy as N
    assert(dim>0)

    # sample radius with p(r)~r^d
    # this can be constructed from a uniform distribution
    u = N.random.uniform(0,1,n)**(1./dim)

    # sample from the surface of a hypersphere
    # see http://mathworld.wolfram.com/HyperspherePointPicking.html
    w = N.random.normal(0,1,(dim,n))
    norm = N.sqrt(N.sum(w*w, axis=0))

    xyz = w/norm*u*radius

    return xyz



if __name__=="__main__":
    test()
