import sys
import matplotlib
matplotlib.rc_context(rc={'backend': 'GTKAgg'})
import pylab

import time

import numpy
import numpy as N
import triangle

from ballpicker import ellipse_picker,ballpicker

import pp

class Walker:
    def __init__(self, mean):
        """ """
        self.mean = mean

    def lnlike(self, x):
        """ n-dimensional gaussian likelihood"""
        sig = 1
        mu = self.mean
    
        k = x.size/len(mu)

        print x

        l = 0.0
        for i in range(len(mu)):
            l += -0.5*numpy.sum( ((x[i] - mu[i])/sig)**2 ) - 0.5*k*numpy.log(2*numpy.pi) - numpy.sum(numpy.log(sig))

        return l

    def draw_sample(self, Lmin, param_limits, point, center, covmat, relax, maxloops=1000,npoints=1):
        """ find a point with higher likelihood by uniform sampling from a hyperellipse """
        trial_points = []
        trial_like = []
        counter = 0
        while len(trial_points)<npoints:
            counter += 1
            if counter>maxloops: break

            # generate a sample that meets the hard prior limits
            loop = 0
            while True:            
                if loop>maxloops:
                    print >>sys.stderr, "**** error: can't find a point inside the prior",loop
                    break
                loop += 1
                trial = ellipse_picker(center, covmat, point=point, relax=relax, n=1)[0]
                point_outside = False
                for i in range(len(trial)):
                    if trial[i]<param_limits[i][0] or  trial[i]>param_limits[i][1]:
                        point_outside=True
                if point_outside: break


            like = self.lnlike(trial)
            if like > Lmin:
                trial_points.append(trial)
                trial_like.append(like)


        accept_rate = len(trial_points)*1./counter

        return trial_points, trial_like, accept_rate


class NestedSampler:
    
    def __init__(self, nparams=1, param_limits=[(-10,10)], walker=None, 
                 nlive=1000, nreject=100, evidence_limit=1e-5, relax=1., 
                 depfuncs=(), modules=(), **ppargs):
        """ 
        nparams -- number of parameters
        param_limits -- hard limits on the prior

        like -- function to evaluate likelihood
        like_args -- arguments required by the function

        nlive -- number of active points
        nreject -- number of points to randomize on each iteration
        evidence_limit -- sets stopping point
        relax -- scale radius for hyperellipse point picker
        
        you may pass in any arguments required by job_server
        """
        self.param_limits = N.array(param_limits)
        self.walker = walker
        #self.lnlike = like
        #self.like_args = like_args
        self.evidence_limit = evidence_limit
        self.relax = relax
        self.nparams = nparams
        self.nlive = nlive
        self.nmod = nreject
        self.all_samples = []
        self.sample_lnlike = []
        self.weights = []

        # arguments for pp
        self.ppargs = ppargs
        self.depfuncs = tuple(list(depfuncs)+[ellipse_picker,ballpicker,self.walker.lnlike])  # add ellipse_picker which is used in draw_sample
        self.modules = modules
        if not 'numpy' in self.modules:  # ensure that numpy is loaded
            self.modules = tuple(list(self.modules) + ['numpy'])
        
        ppservers = ()
        ncpus = 'autodetect'
        if ppargs.has_key('ppservers'): ppservers = ppargs['ppservers']
        if ppargs.has_key('ncpu'): ncpu = ppargs['ncpu']

        self.job_server = pp.Server(ncpus=ncpus,ppservers=ppservers)

        # print the number of cpus found
        print "Starting pp with", self.job_server.get_ncpus(), "workers"




    def go(self):
        """ do it """

        print "~~~~ starting nested sampler ~~~~~~`"
        print "     nlive = %i"%self.nlive
        print "  n reject = %i"%self.nmod

        # generate initial sampling over a uniform prior
        assert(len(self.param_limits)==self.nparams)
        livepoints = []
        for i in range(self.nparams):
            a,b = self.param_limits[i]
            livepoints.append(  N.random.uniform(a,b,(self.nlive)) )
        livepoints = N.transpose(livepoints)

        # compute the likelihood at these points
        print ">> computing initial likelihood values"
        lnlike_vals = N.zeros(self.nlive)
        jobs = []
        for i in range(self.nlive):
            args = [livepoints[i]]
            jobs.append( self.job_server.submit(self.walker.lnlike, args=tuple(args), 
                                                depfuncs=self.depfuncs, modules=self.modules,  **self.ppargs) )
        

        # collect results
        for i in range(self.nlive):
            lnlike_vals[i] = jobs[i]()


        self.job_server.print_stats()


        print ">> now running in loops"

        count = 0
        covlist = []
        like_list = []
        evidence = 10000
        evidence_next = 0
        remaining_evidence = 10000

        # this is the step accounting for nreject points
        # (see Henderson & Goggans 2014)
        logstep = N.sum(1./(self.nlive - N.arange(self.nmod)))

        stop = False

        while  N.abs(remaining_evidence) > self.evidence_limit:
            count += 1

            # print a helpful message
            if not count%100: 
                print "~~~~~~~~~ iteration",count
                print "evidence:",evidence*1./tot_evidence
                print "remaining",remaining_evidence
                print "last sample",livepoints[ind_lowest]

        
            # identify the n lowest likelihood points
            ind_low = N.argpartition(lnlike_vals, self.nmod)[:self.nmod]
            ind_lowest = ind_low[-1]

            # compute p step
            step = N.exp(-(count-1)*logstep) - N.exp(-count*logstep)

            # compute statistics of the live points
            mean = N.mean(livepoints, axis=0)
            cov = N.cov(livepoints, rowvar=0)

            # draw sample with higher likelihood using pp job server
            args = (lnlike_vals[ind_lowest],self.param_limits, livepoints[ind_lowest], mean, cov, self.relax)
            jobs = []
            for i in range(self.nmod):
                jobs.append( self.job_server.submit(self.walker.draw_sample, args, 
                                                    depfuncs=self.depfuncs, modules=self.modules, **self.ppargs))

            # collect results
            trial_points = []
            trial_like = []
            accept_rate = []
            for job in jobs:
                res = job()
                p,l,r = res
                if len(p)==0:
                    stop = True
                    print "********* warning! unable to draw next point", cov,"***********"
                    break
                trial_points.append(p[0])
                trial_like.append(l[0])
                accept_rate.append(r)

            print "accept rate",N.mean(accept_rate)


            if stop:
                break

            w = step*N.exp(lnlike_vals[ind_low])
            if count==1:
                self.all_samples = livepoints[ind_low]
            else:
                self.all_samples = N.vstack([self.all_samples, livepoints[ind_low]])

            self.weights = N.concatenate([self.weights,w])
            self.sample_lnlike = N.concatenate([self.sample_lnlike, lnlike_vals[ind_low]])


            # update the live points with new draw
            for i in range(self.nmod):
                livepoints[ind_low[i]] = trial_points[i]
                lnlike_vals[ind_low[i]] = trial_like[i]


            # keep count of the evidence
            evidence += N.sum(N.exp(self.sample_lnlike)*self.weights)
            remaining_evidence = N.exp(N.max(lnlike_vals))*N.exp(-count*1./self.nlive)
            tot_evidence = evidence + remaining_evidence
            
            remaining_evidence /= tot_evidence

            assert(len(livepoints)==self.nlive)


        # weight remaining points
        # sort by increasing likelihood
        order = N.argsort(lnlike_vals)
        lnlike_vals = lnlike_vals[order]
        livepoints = livepoints[order]

        # compute ranks and weight
        rank = N.arange(len(lnlike_vals))
        x = N.exp(-rank*1./self.nlive)
        step = x[:-1]-x[1:]
        step = N.concatenate([step,[step[-1]]])

        w = step*N.exp(lnlike_vals)
        if w[0]>0:
            w *= self.weights[-1]/w[0]   # not sure about normalization, but match it to previously computed weights
        else:
            w *= 0.

        self.all_samples = N.vstack([self.all_samples, livepoints])
        self.weights = N.concatenate([self.weights,w])
        self.sample_lnlike = N.concatenate([self.sample_lnlike, lnlike_vals])


        # normalize weights by total evidence
        tot = N.sum(self.weights*N.exp(self.sample_lnlike))
        self.weights /= tot

        self.evidence = tot

        print "*** total evidence", self.evidence

        # pylab.title("lnlike")
        # pylab.plot(self.sample_lnlike)
        # pylab.figure()

        # pylab.title("weight")
        # pylab.plot(N.cumsum(self.weights*N.exp(self.sample_lnlike)))

        # pylab.plot(self.weights)

        # pylab.figure()

        print " >> finished %i loops"%count
        print " >> got this many samples:",len(self.all_samples)

        self.job_server.print_stats()


    def best(self):
        """ """
        mu = N.average(self.all_samples, weights=self.weights, axis=0)
        var =  N.average((self.all_samples - mu)**2, weights=self.weights, axis=0)

        var2 =  N.average((self.all_samples[:,:100] - mu)**2, axis=0)


        print "---- best ----"
        for i in range(len(mu)):
            print mu[i],var2[i]**.5,var[i]**.5

        return mu,var**.5

def test(dim=1):
    """ """

    import matplotlib
    matplotlib.rc_context(rc={'backend': 'GTKAgg'})
    import pylab



    npoints = 1
    sigma = N.ones((dim,npoints))
    mu = N.ones((dim,npoints))


    walker = Walker(mu)


    param_limits = []
    for i in range(dim):
        param_limits.append((-5,6))

    Nest = NestedSampler(dim, param_limits, walker=walker)
    

    Nest.go()
    mu,sig = Nest.best()
    
    print "should be",sigma[0]/N.sqrt(npoints)


    chain = Nest.all_samples
    weights = Nest.weights


    ext = []
    for i in range(len(mu)):
        ext.append((mu[i]-3*sig[i], mu[i]+3*sig[i]))

    hw,e=N.histogram(chain.T[0],weights=weights,bins=N.linspace(ext[0][0],ext[0][1],50),density=True)
    pylab.plot(e[:-1],hw)


    h,e2=N.histogram(chain.T[0],bins=N.linspace(ext[0][0],ext[0][1],50),density=True)
    pylab.plot(e2[:-1],h)

    x = N.linspace(-2,3,100)

    lny = []
    for i in range(len(x)):
        lny.append(walker.lnlike( N.array([x[i]]) ) )

    pylab.semilogy(x,N.exp(lny),dashes=[4,3],c='k')
    

    pylab.show()







if __name__=="__main__":
    test()
