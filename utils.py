import numpy
import pylab
import scipy.ndimage as ndi

def draw_likelihood(p1, p2, weights, ax):

    x0 = min(p1)
    x1 = max(p1)

    y0 = min(p2)
    y1 = max(p2)

    print x0,x1,y0,y1

    data = zip(p1, p2)

    img = grid_density_gaussian_filter(x0, y0, x1, y1, 512, 512, data, weights)

    ax.imshow(img, origin='lower', extent=[x0, x1, y0, y1], aspect='auto')


# function to do a gaussian smoothing to make nice plots
def grid_density_gaussian_filter(x0, y0, x1, y1, w, h, data, weights):
    kx = (w - 1) / (x1 - x0)
    ky = (h - 1) / (y1 - y0)
    r = 20
    border = r
    imgw = (w + 2 * border)
    imgh = (h + 2 * border)
    img = numpy.zeros((imgh,imgw))
    i = 0
    for x, y in data:
        ix = int((x - x0) * kx) + border
        iy = int((y - y0) * ky) + border
        if 0 <= ix < imgw and 0 <= iy < imgh:
            img[iy][ix] += weights[i]
        i +=1
    return ndi.gaussian_filter(img, (r,r))  ## gaussian convolution                                                                                               
