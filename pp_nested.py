import math, sys, time
import pp
import numpy
import scipy
import scipy.ndimage as ndi
import matplotlib.pyplot as plt
import pp_voids

print """Usage: python pp_nested.py [ncpus]                                     
    [ncpus] - the number of workers to run in parallel,                        
    if omitted it will be set to the number of processors in the system

    performs a nested sampling fit to the void-galaxy cross correlation
"""

# tuple of all parallel python servers to connect with                         
ppservers = ()

if len(sys.argv) > 1:
    ncpus = int(sys.argv[1])
    # Creates jobserver with ncpus workers                                     
    job_server = pp.Server(ncpus, ppservers=ppservers)
else:
    # Creates jobserver with automatically detected number of workers          
    job_server = pp.Server(ppservers=ppservers)

# print the number of cpus found
print "Starting pp with", job_server.get_ncpus(), "workers"

# start with box priors
# redshift space distortion parameter
beta_min = 0.2
beta_max = 1.0

# central density parameter
dc_min = -1.0
dc_max = -0.4

# stretch radius parameter
rv_min = 0.7
rv_max = 1.2

# shape parameter
alpha_min = 2.0
alpha_max = 4.0

# if any of these priors are 'hard' then need to include this info

# some parameters
nmod = 600 # number of points used to build the model
nlive = 2*nmod # initial number of points  
nparams = 4 # there are four parameters in the model
relax = 1.05 # expands the gaussian slightly to slow the algorithm

evidence_limit = 1e-12 # the precision at which to terminate the chain
# this should be a very small number


# draw initial points from a uniform distribution

beta_vals = numpy.random.uniform(beta_min, beta_max, nlive)
dc_vals = numpy.random.uniform(dc_min, dc_max, nlive)
rv_vals = numpy.random.uniform(rv_min, rv_max, nlive)
alpha_vals = numpy.random.uniform(alpha_min, alpha_max, nlive)

# initiate the array which will contain all the parameter values and their likelihoods
all_samples = []

# load in stuff that needs to be loaded in
print "loading in covariance matrix"
covmat_file = str(sys.argv[2])
covmat = pp_voids.load_covmat(covmat_file)

# invert covariance matrix                                                                                                                                       
#cov_mat_inv = numpy.linalg.inv(covmat)
# apply correction                                                                                                                                              
#nb = 100 #number of bins                                                                                                                                        
#np = 4 #number of parameters                                                                                                                                    
#ns = 200 #number of mocks                                                                                                                                       
#D = (nb + 1.0)/(ns - 1.0)
#cov_mat_inv = (1-D)*cov_mat_inv
#detcovmatinv = numpy.linalg.det(cov_mat_inv)
#print detcovmatinv

print "covariance matrix loaded"
print "loading in cross correlation file"

ccorr_file = str(sys.argv[3])
xi_si_pi = pp_voids.load_data(ccorr_file)
print "cross correlation loaded"
r_vals = numpy.asarray([0.3*i for i in range(10)])

# load the inputs into an array    
inputs = [(beta_vals[i], dc_vals[i], rv_vals[i], alpha_vals[i], r_vals, xi_si_pi, covmat) for i in range(nlive)]

# submit the jobs to the workers
# functions and modules used need to be declared here
# consult the pp documentation as to how to do this properly
jobs = [(input, job_server.submit(pp_voids.lnlike,(input,), (pp_voids.scaled_exponential_profile,),("import math","numpy","import pp_voids",))) for input in inputs]
print "running for initial set of ",nlive," points"
for input, job in jobs:
    res = job()
    #print "likelihood", input, "is", res
    beta,dc,rv,alpha,r_vals_,xi_si_pi_,covmat_ = input
    all_samples.append([beta, dc, rv, alpha, res])

# convert simple list to a numpy object
all_samples = numpy.asarray(all_samples)

ev_left = 100000 # set the initial amount of evidence left at some arbitrarily high value

# order the array by decreasing likelihood
all_samples = all_samples[numpy.argsort(all_samples[:,nparams])]
all_samples = all_samples[::-1]
#print all_samples

## take the nmod most likely points
# these are used to build the gaussian model for the posterior
print "taking the ",nmod," most likely points"
beta_vals = numpy.arange(nmod)*0.0
dc_vals = numpy.arange(nmod)*0.0
rv_vals = numpy.arange(nmod)*0.0
alpha_vals = numpy.arange(nmod)*0.0

saved_points = all_samples[:nmod]
for i in range(nmod): 
    beta_vals[i] = saved_points[i][0]
    dc_vals[i] = saved_points[i][1]
    rv_vals[i] = saved_points[i][2]
    alpha_vals[i] = saved_points[i][3]

data = [beta_vals, dc_vals, rv_vals, alpha_vals]

# now start the nested sampling
# terminate chain when it has reached desired evidence limit
while ev_left >= evidence_limit:
    
## draw multivariate gaussian around these points
    mean = numpy.mean(data, axis=1)
    cov = numpy.cov(data, rowvar=1)*relax

## draw some random points from inside this Gaussian
    trial_points = numpy.random.multivariate_normal(mean,cov,nlive).T
    [beta_vals, dc_vals, rv_vals, alpha_vals] = trial_points

    # put in some hard priors 
    for i in range(nlive):
        if dc_vals[i] >=0:
            dc_vals[i] = -dc_vals[i] # central density must be negative
        if dc_vals[i] <-1:
            dc_vals[i] = dc_vals[i]+1 # central density cannot be less than -1
        if rv_vals[i] <=0:
            rv_vals[i] = -rv_vals[i] # shape parameter cannot be negative

    inputs = [(beta_vals[i], dc_vals[i], rv_vals[i], alpha_vals[i], r_vals, xi_si_pi, covmat) for i in range(nlive)]
    # run next round of jobs
    print "running next round of jobs"
    jobs = [(input, job_server.submit(pp_voids.lnlike,(input,), (pp_voids.scaled_exponential_profile,),("import math","numpy","import pp_voids",))) for input in inputs]
    for input, job in jobs:
        res = job()
        beta,dc,rv,alpha,r_vals_,xi_si_pi_,covmat_ = input
        all_samples = numpy.append(all_samples, [[beta, dc, rv, alpha, res]], axis=0)

    # sort the samples again
    all_samples = all_samples[numpy.argsort(all_samples[:,nparams])]
    all_samples = all_samples[::-1]
    chisq_vals = -2.0*all_samples[:,nparams]
    #print chisq_vals[:5]
 
    like_vals = numpy.exp(-chisq_vals/2.0)
    #like_vals = (detcovmatinv/numpy.sqrt(2*numpy.pi))*numpy.exp(-chisq_vals/2.0)
    index = 1 + numpy.arange(chisq_vals.size)

    # this is an import parameter
    # look in Feroz and Hobsen
    X_vals = numpy.exp(-index*1.0/nmod)
    like_weights = []

    # calculate nested sampling variables      
    # the likelihood weights
    for i in range(chisq_vals.size):
        if i == 0:
            like_weight = 0.5*(-X_vals[i+2]+X_vals[i])
        if i == chisq_vals.size-1:
            like_weight = 0.5*(-X_vals[i]+X_vals[i-2])
        else:
            like_weight = 0.5*(-X_vals[i+1]+X_vals[i-1])
        like_weights.append(like_weight)

    like_weights[0] = like_weights[1] # fix bug

    like_weights = numpy.asarray(like_weights)

    # the evidence thus far covered
    evidence = numpy.sum(like_vals*like_weights)

    print 'evidence = ', evidence

    print 'max like = ', max(like_vals)

    # the estimated evidence remaining
    remaining_evidence = max(like_vals)*min(X_vals)
    ev_left = remaining_evidence
    print "remaining evidence = ", ev_left
    print "accuracy is ", remaining_evidence/evidence

    # the point weights
    # this may actually be different for gaussian samples
    point_weights = like_vals*like_weights/evidence

## take the 50 most likely points
# these are then used to model the posterior
    beta_vals = numpy.arange(nmod)*0.0
    dc_vals = numpy.arange(nmod)*0.0
    rv_vals = numpy.arange(nmod)*0.0
    alpha_vals = numpy.arange(nmod)*0.0

    saved_points = all_samples[:nmod]
    for i in range(nmod):
        beta_vals[i] = saved_points[i][0]
        dc_vals[i] = saved_points[i][1]
        rv_vals[i] = saved_points[i][2]
        alpha_vals[i] = saved_points[i][3]

    data = [beta_vals, dc_vals, rv_vals, alpha_vals]



# now the chain has terminated let's plot the results 
# the parameter estimates and their errors                         
def weighted_avg_and_std(values, weights):
    """
    Return the weighted average and standard deviation.

    values, weights -- Numpy ndarrays with the same shape.
    """
    average = numpy.average(values, weights=weights)
    variance = numpy.average((values-average)**2, weights=weights)  # Fast and numerically precise
    return (average, math.sqrt(variance))

[beta_av, beta_err] = weighted_avg_and_std(all_samples[:,0], point_weights)
[dc_av, dc_err] = weighted_avg_and_std(all_samples[:,1], point_weights)
[rv_av, rv_err] = weighted_avg_and_std(all_samples[:,2], point_weights)
[alpha_av, alpha_err] = weighted_avg_and_std(all_samples[:,3], point_weights)

print "weighted avaerage of beta = ", beta_av, " weighted STD = ", beta_err
print "weighted avaerage of dc = ", dc_av, " weighted STD = ", dc_err
print "weighted avaerage of rv = ", rv_av, " weighted STD = ", rv_err
print "weighted avaerage of alpha = ", alpha_av, " weighted STD = ", alpha_err

# function to do a gaussian smoothing to make nice plots
def grid_density_gaussian_filter(x0, y0, x1, y1, w, h, data, weights):
    kx = (w - 1) / (x1 - x0)
    ky = (h - 1) / (y1 - y0)
    r = 20
    border = r
    imgw = (w + 2 * border)
    imgh = (h + 2 * border)
    img = numpy.zeros((imgh,imgw))
    i = 0
    for x, y in data:
        ix = int((x - x0) * kx) + border
        iy = int((y - y0) * ky) + border
        if 0 <= ix < imgw and 0 <= iy < imgh:
            img[iy][ix] += weights[i]
        i +=1
    return ndi.gaussian_filter(img, (r,r))  ## gaussian convolution                                                                                               

# draw the likelihood in a 2d plane
def draw_likelihood(p1, p2, x0, x1, y0, y1, weights, ax):

    #x0 = min(p1)
    #x1 = max(p1)

    #y0 = min(p2)
    #y1 = max(p2)

    data = zip(p1, p2)

    img = grid_density_gaussian_filter(x0, y0, x1, y1, 256, 256, data, weights)

    ax.imshow(img, origin='lower', extent=[x0, x1, y0, y1], aspect='auto')


print "drawing plots"

f, axarr = plt.subplots(4, 4)

#all_samples = all_samples[:100]
#point_weights = point_weights[:100]

param1_vals = all_samples[:,0]
param2_vals = all_samples[:,1]
param3_vals = all_samples[:,2]
param4_vals = all_samples[:,3]

param1_min = 0.5
param2_min = -1.0
param3_min = 0.65
param4_min = 2.0

param1_max = 1.3
param2_max = -0.5
param3_max = 1.15
param4_max = 4.5

im1 = draw_likelihood(param1_vals,param2_vals, param1_min, param1_max, param2_min, param2_max, point_weights, axarr[1, 0])
im2 = draw_likelihood(param1_vals,param3_vals, param1_min, param1_max, param3_min, param3_max, point_weights, axarr[2, 0])
im3 = draw_likelihood(param1_vals,param4_vals, param1_min, param1_max, param4_min, param4_max, point_weights, axarr[3, 0])
im4 = draw_likelihood(param2_vals,param3_vals, param2_min, param2_max, param3_min, param3_max, point_weights, axarr[2, 1])
im5 = draw_likelihood(param2_vals,param4_vals, param2_min, param2_max, param4_min, param4_max, point_weights, axarr[3, 1])
im6 = draw_likelihood(param3_vals,param4_vals, param3_min, param3_max, param4_min, param4_max, point_weights, axarr[3, 2])

# plot histograms
axarr[0, 0].hist(param1_vals, bins=50, normed=True, weights=point_weights)
axarr[0, 0].set_xlim([param1_min,param1_max])
# pverplot planck prediction and other vipers measurement
axarr[0, 0].plot([0.81939468, 0.81939468], [0,5], 'k', label="Planck", linewidth=3)
axarr[0, 0].plot([0.8854621612101197, 0.8854621612101197], [0,5], 'r', label="VIPERS (de la Torre et al)", linewidth=3)
axarr[0, 0].plot([1.036179124820353, 1.036179124820353], [0,5], 'r--')
axarr[0, 0].plot([0.7347451975998865, 0.7347451975998865], [0,5], 'r--')
axarr[0, 0].text(0.2, 0.9, r'$\beta = $'+str(float('%.3g' % beta_av)), horizontalalignment='center', verticalalignment='center', transform = axarr[0,0].transAxes)
axarr[0, 0].text(0.2, 0.8, r'$\sigma_\beta = $'+str(float('%.2g' % beta_err)), horizontalalignment='center', verticalalignment='center', transform = axarr[0,0].transAxes)
#axarr[0, 0].text(0.8, 0.9, r'$\beta=$'+str(float('%.4g' % beta_av))+str('\n')+'$\sigma_\beta = $'+str(float('%.4g' % beta_err)), horizontalalignment='center', verticalalignment='center', transform = axarr[0,0].transAxes)
#axarr[0,0].legend(loc=6)

axarr[1, 1].hist(param2_vals, bins=50, normed=True, weights=point_weights)
axarr[1, 1].set_xlim([param2_min,param2_max])
axarr[1, 1].text(0.2, 0.9, r'$\delta_c = $'+str(float('%.3g' % dc_av)), horizontalalignment='center', verticalalignment='center', transform = axarr[1,1].transAxes)
axarr[1, 1].text(0.2, 0.8, r'$\sigma_{\delta_c} = $'+str(float('%.2g' % dc_err)), horizontalalignment='center', verticalalignment='center', transform = axarr[1,1].transAxes)

axarr[2, 2].hist(param3_vals, bins=50, normed=True, weights=point_weights)
axarr[2, 2].set_xlim([param3_min,param3_max])
axarr[2, 2].text(0.2, 0.9, r'$r_v = $'+str(float('%.3g' % rv_av)), horizontalalignment='center', verticalalignment='center', transform = axarr[2,2].transAxes)
axarr[2, 2].text(0.2, 0.8, r'$\sigma_{r_v} = $'+str(float('%.2g' % rv_err)), horizontalalignment='center', verticalalignment='center', transform = axarr[2,2].transAxes)

axarr[3, 3].hist(param4_vals, bins=50, normed=True, weights=point_weights)
axarr[3, 3].set_xlim([param4_min,param4_max])
axarr[3, 3].text(0.2, 0.9, r'$\alpha = $'+str(float('%.3g' % alpha_av)), horizontalalignment='center', verticalalignment='center', transform = axarr[3,3].transAxes)
axarr[3, 3].text(0.2, 0.8, r'$\sigma_\alpha = $'+str(float('%.2g' % alpha_err)), horizontalalignment='center', verticalalignment='center', transform = axarr[3,3].transAxes)


axarr[0, 1].get_axes().set_visible(False)
axarr[0, 2].get_axes().set_visible(False)
axarr[0, 3].get_axes().set_visible(False)
axarr[1, 2].get_axes().set_visible(False)
axarr[1, 3].get_axes().set_visible(False)
axarr[2, 3].get_axes().set_visible(False)

axarr[1, 0].get_xaxis().set_visible(False)
axarr[2, 0].get_xaxis().set_visible(False)
axarr[2, 1].get_xaxis().set_visible(False)

axarr[2, 1].get_yaxis().set_visible(False)
axarr[3, 1].get_yaxis().set_visible(False)
axarr[3, 2].get_yaxis().set_visible(False)

axarr[0, 0].get_xaxis().set_visible(False)
axarr[1, 1].get_xaxis().set_visible(False)
axarr[2, 2].get_xaxis().set_visible(False)

axarr[3, 3].set_xlabel(r'$\alpha$')
axarr[3, 2].set_xlabel(r'$r_v$')
axarr[3, 1].set_xlabel(r'$\delta_c$')
axarr[3, 0].set_xlabel(r'$\beta$')

axarr[3, 0].set_ylabel(r'$\alpha$')
axarr[2, 0].set_ylabel(r'$r_v$')
axarr[1, 0].set_ylabel(r'$\delta_c$')

axarr[0, 0].plot([0.81939468, 0.81939468], [0,5], label="Planck", linewidth=3)
axarr[0, 0].plot([0.8854621612101197, 0.8854621612101197], [0,5], 'k', label="VIPERS (de la Torre et al)", linewidth=3)
axarr[0, 0].plot([1.036179124820353, 1.036179124820353], [0,5], 'k--')
axarr[0, 0].plot([0.7347451975998865, 0.7347451975998865], [0,5], 'k--')

#axarr[0,0].legend(loc=6)

plt.show()


